-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2018 at 11:43 AM
-- Server version: 5.5.40
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_aplikasisehat`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel`
--

CREATE TABLE IF NOT EXISTS `tb_artikel` (
  `artikel_id` int(100) NOT NULL,
  `artikel_judul` varchar(100) NOT NULL,
  `artikel_isi` longtext NOT NULL,
  `artikel_tanggal` varchar(100) NOT NULL,
  `artikel_gambar` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_artikel`
--

INSERT INTO `tb_artikel` (`artikel_id`, `artikel_judul`, `artikel_isi`, `artikel_tanggal`, `artikel_gambar`) VALUES
(1, 'Studi: Pangkas Kalori 15 Persen Bikin Awet Muda!', 'Ngomongin kalori, kaitannya nggak cuma dengan berat badan. Menurut penelitian terbaru, memangkas asupan kalori hingga 15 persen selama dua tahun bisa memperlambat proses penuaan.  \r\n"Jadi kami tidak dapat menyimpulkan bahwa perubahan ini secara hubungannya terkait dengan berkurangnya penuaan," jelasnya.\r\nStudi yang dipublikasikan di Journal Cell Metabolism tersebut mengatakan bahwa asupan kalori berpengaruh pula pada berbagai risiko penyakit yang terkait metabolisme. Termasuk di antaranya diabetes, kanker, dan alzheimer yang merupakan salah satu jenis pikun.\r\n', '2018-12-06T05:34:39.282Z', '2.jpeg'),
(2, 'Tes HIV-AIDS dan Obatnya Tersedia Gratis!', 'Kementerian Kesehatan (Kemenkes) menegaskan tes dan obat Human Immunodeficieny Virus (HIV) Acquired Immune Deficiency Syndrome (AIDS) tersedia gratis. Anggota populasi kunci dan terdampak tak perlu merogoh kocek sendiri untuk mengetahui statusnya, serta untuk mendapat penanganan. Terapi HIV dan AIDS terdiri atas tes CD 4 dan viral load (VL) untuk penegakan diagnosis, serta obat antiretroviral (ARV).\r\n\r\n"Tes HIV AIDS bisa diperoleh gratis di 5.000 fasilitas kesehatan di seluruh Indonesia. Sekitar 3.000 adalah puskesmas yang aksesnya lebih dekat dengan masyarakat," kata Kepala Sub Direktorat HIV AIDS dan Penyakit Infeksi Menular Seksual (PIMS) Kemenkes Endang Budi Hastuti pada Senin (3/12/2018).\r\n\r\nKemenkes memperkirakan ada 630 ribu orang dengan HIV-AIDS (ODHA) di 2018. Dari jumlah tersebut, hanya 48 persen yang mengetahui statusnya. Jumlah yang masih menjalani terapi ARV hanya 30 persen, sedangkan kuranh dari 10 persen memilih dites viral load. Akibatnya hanya 0,64 persen orang yang berhasil menekan jumlah HIV di dalam tubuhnya. Capaian ini masih jauh dari target global yang mensyaratkan 90 persen ODHA tahu statusnya, 90 persen menjalani pengobatan, dan 90 persen berhasil menekan jumlah HIV.\r\n', '2018-12-06T05:34:39.282Z', '1.jpeg'),
(3, 'Kenali Bahaya Hipoglikemia pada ', 'Diabetes atau kencing manis tidak melulu berurusan dengan kadar gula darah yang tinggi. Pada kondisi tertentu, pasien juga mengalami penurunan kadar gula di bawah normal, yang dikenal dengan istilah hipoglikemia. Sama bahayanya dengan gula darah tinggi, hipoglikemia atau gula darah rendah juga bisa merusak tubuh. "Kalau misalkan berlangsung berulang-ulang dan terus menerus, maka bisa menyebabkan kerusakan terutama di bagian otak," kata dr Mochammad Pasha SpPD dari RS Pusat Pertamia, dalam sebuah diskusi di Kebayoran Baru, Jakarta Selatan baru-baru ini.\r\n\r\nMenurut dr Pasha, gula adalah makanan utama bagi otak. Hipoglikemia menyebabkan otak kekurangan pasokan makanan sehingga sel-sel otak mengalami kerusakan dan bisa menyebabkan pikun maupun gangguan kognitif.Gejala hipiglikemia bisa dikenali dari tubuh gemetar, jantung berdebar-debar, keringat berlebih, dan pandangan kabur. Gejala tersebut akan memburuk jika hipoglikemia tidak segera mendapat penanganan yang tepat.\r\n', '2018-12-06T05:34:39.282Z', '3.jpeg'),
(4, 'Optimalkan Imunisasi MR di Aceh, MUI Ingin Kirim Komisi Fatwa ', 'Angka anak yang melakukan imunisasi di Aceh masih terendah di Indonesia. Berdasarkan data terakhir per 10 September 2018 dari Kementerian Kesehatan (Kemenkes) RI, baru sekitar 4.94 persen anak atau sejumlah 76.461 anak yang melaksanakan vaksinasi MR di Aceh.\r\n\r\nMenambah peliknya urusan vaksin MR, Aceh kini juga tengah melakukan penundaan terhadap vaksin MR yang menggunakan vaksin buatan Serum Institute of India (SII) belum memakai bahan halal. Ini tidak membuat Majelis Ulama Indonesia (MUI) diam, Ketua Umum MUI, Ma''ruf Amin, menyebutkan pihaknya akan berupaya sebisa mungkin membuka dialog dengan Majelis Permusyawaratan Ulama (MPU) Aceh mengenai vaksin MR."Karena Aceh punya undang-undang otonomi dia pakai nama MPU, nanti kita akan ajak diskusi alasan menolak apa, kenapa menolak? Kalau bisa komisi fatwa akan kita kirim ke Aceh untuk berdiskusi membahas imunisasi dan kebolehan meng gunakan vaksin MR, saya yakin enggak lama lagi akan selesai," kata KH Ma''ruf Amin.\r\n', '2018-12-06T05:34:39.282Z', '4.jpeg'),
(5, 'Kata Dokter Paru Soal Anggapan Perokok ''Selamatkan'' BPJS Kesehatan(', 'Perhimpunan Dokter Paru Indonesia (PDPI) menyatakan, besaran cukai dan total ongkos pengobatan akibat rokok tak sebanding. Besaran cukai lebih kecil daripada biaya kuratif yang kini menjadi tanggungan BPJS Kesehatan. Beberapa penyakit akibat rokok yaitu gangguan jantung, stroke, dan kanker tercatat paling banyak menghabiskan dana BPJS Kesehatan tiap tahun.Potensi cukai rokok sebesar Rp 1,48 triliun hanya bisa menyelematkan BPJS Kesehatan sementara waktu. Dengan fakta tersebut, tak tepat bila dikatakan cukai rokok telah menyelamatkan BPJS Kesehatan.\r\n\r\n"Statement seperti ini perlu diluruskan," kata dokter ahli paru yang juga ketua PDPI Agus Dwi Susanto, Jumat (21/9/2018).\r\n\r\nHal ini mengindikasikan, jumlah perokok bisa terus meningkat tiap tahunnya. Sementara, penggunaan dana cukai hanya menutup sebagian dari dampak buruk merokok. Hal ini mengindikasikan, besar kemungkinan BPJS Kesehatan akan mengalami kondisi serupa saat ini di kemudian hari akibat rokok.\r\n', '2018-12-06T05:34:39.282Z', '5.jpeg'),
(6, 'Terbanyak Pengidapnya di Indonesia, Begini Cara Penularan Hepatitis B(', 'Penyakit hepatitis B diidap oleh 21,8 persen penduduk di Indonesia menurut data Riset Kesehatan Dasar (Riskesdas) tahun 2013. Sehingga menjadikannya salah satu tipe hepatitis terbanyak diidap dan Indonesia sebagai salah satu negara dengan pengidap hepatitis B terbanyak di dunia.\r\n\r\n"Horizontal itu kalau saya donor darah, tidak sengaja donornya dikasih ke orang lain, tertularlah dia dengan hepatitis B. Atau di rumah sakit saya diambil darah, jarumnya ketusuk ke perawat atau ke dokter. Atau hubungan seksual," terang dr Irsan Hasan, SpPD-KGEH dari RS Cipto Mangunkusumo.\r\n\r\nPencegahan hepatitis B di Indonesia sudah dicanangkan oleh Kementerian Kesehatan RI lewat program wajib vaksinasi ibu hamil dan balita mulai usia 1 tahun. Organisasi Kesehatan Dunia (WHO) menyebutkan setidaknya 257 juta orang di dunia mengidap hepatitis B dan 887 ribu kematian terjadi di tahun 2015. Hepatitis B kadang tanpa gejala, dapat menyebabkan baik penyakit akut maupun kronis seperti hepatitis D, sirosis dan kanker hati (hepatocellular carcinoma).\r\n', '2018-12-06T05:34:39.282Z', '6.jpg'),
(7, 'Terapi Pangan Lokal Bantu Jaga Stamina Tubuh', 'Kementerian Pertanian (Kementan) melalui Direktorat Jenderal Hortikultura menggelar Sosialisasi Manfaat Pangan Lokal untuk Stamina Tubuh dengan bantuan teknologi. Sosialisasi bekerja sama dengan HanaRa Clinic yang dikelola, dr. Henson Barki.\n\ndr. Hanson menjelaskan kliniknya menggunakan teknologi yang disebut HanaRa Life Energy Pool dan Fountain untuk membangun stamina tubuh. Teknologi tersebut merupakan teknologi air terkini yang dibuat sesuai prinsip Body Intelligence. Fasilitas penerima 3 kali penghargaan rekor dunia dari MURI ini baru ada di Bandung dan merupakan fasilitas pertama di dunia. Teknologi ini digunakan HanaRa untuk mempercepat pencapaian potensi tubuh secara optimal. \n\n"Organ vital pada tubuh manusia bisa mengganggu emosi. Kelima organ vital tersebut meliputi limpa, ginjal, liver, jantung, dan paru-paru yang kondisinya sangat dipengaruhi asupan makanan. Pangan lokal bisa membangun kerja yang positif kelima organ ini," ujar dr. Hanson dalam keterangan tertulis\n', '2018-12-06T05:34:39.282Z', '7.jpg'),
(8, 'Mengenal 2 Obat Hipertensi yang Ditarik di AS Karena Terkait Kanker', 'Penarikan sukarela di Amerika Serikat hingga ke tingkat pasien untuk semua tablet kombinasi Amlodipine/Valsartan dan Amlodipine/Valsartan/Hydrochlorothiazide yang diproduksi oleh Mylan India telah dilakukan oleh Teva Pharmaceutical. Penyebabnya karena ditemukan pengotor N-nitroso-diethylamine (NDEA) yang memungkinkan jadi karsinogen bagi manusia.\n\n"Valsartan itu obat darah tinggi, obat gagal jantung yang bagus, penelitian menunjukan itu bagus dan menurunkan kematian, menurunkan morbiditas, pengontrol tensi yang baik, demikian Amlodipine. Nah sedangkan hydrochlorothiazide itu golongan diuretik, itu juga bagus pada orangtua yang sering kali mengalami darah tinggi dan tidak bisa dikontrol dengan satu macam obat. Jadi dibutuhkan kombinasi," ungkapnya Prof Dr dr Yoga Yuniadi, SpJP(K) saat dihubungi melalui saluran telepon', '2018-12-06T05:34:39.282Z', '8.jpg'),
(9, 'Berbagai Penyebab Ngorok, Mulai dari Posisi Tidur Hingga Obesitas', 'Ngorok atau mendengkur adalah kondisi umum yang dapat terjadi pada siapa saja, terutama pada pria. Kebiasaan mendengkur akan cenderung memburuk seiring dengan bertambahnya usia.\n\nJika mendengkur hanya sesekali, biasanya tidak terlalu serius dan tidak membutuhkan penanganan lanjutan. Tetapi jika berlangsung secara terus menerus, nggak cuma mengganggu orang lain tetapi kebiasaan ini juga merusak kualitas tidur dan membuat waktu istirahat menjadi lebih sedikit.Mendengkur terjadi ketika aliran udara terhambat sehingga asupan oksigen menjadi berkurang. \n', '2018-12-06T05:34:39.282Z', '9.jpeg'),
(10, '5 Makanan yang Secara Alami Punya Kandungan ''Formalin'' ', 'Formalin atau formaldehide memang identik sebagai bahan kimia pengawet makanan yang berbahaya bagi kesehatan. Namun sebetulnya, formalin ditemukan secara alami dalam beberapa produk pangan. \r\n\r\nSesuai ketentuan International Programme on Chemical Safety (IPCS), ambang batas aman formalin adalah 1 miligram per liter. Formalin yang kurang dari jumlah tersebut tidak berisiko mengganggu fungsi dan kesehatan tubuh.Selain anggur, berikut deretan buah dan sayur yang mengandung formalin dikutip dari situs Centre Food Safety milik pemerintah Hongkong. \r\n', '2018-12-06T05:34:39.282Z', '10.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_diskusi`
--

CREATE TABLE IF NOT EXISTS `tb_diskusi` (
  `id_diskusi` int(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `diskusi` text NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_diskusi`
--

INSERT INTO `tb_diskusi` (`id_diskusi`, `nama`, `email`, `tanggal`, `diskusi`, `foto`) VALUES
(10, 'Mohammad Ichwan Rizky Kurnia', 'ichwanrizky123@gmail.com', '2018-12-14T07:25:19.336Z', 'Ada yang pernah mengalami demam tinggi hingga berhari hari ??? boleh minta tips nya tidak untuk mengobatinya dengan cara alternatif', 'profile1.PNG'),
(11, 'ragilia panca kurniasa', 'ragiliapanca5@gmail.com', '2018-12-17T07:19:49.692Z', 'ada tips buat menjaga nafsu makan ???', 'caca.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_diskusi_reply`
--

CREATE TABLE IF NOT EXISTS `tb_diskusi_reply` (
  `diskusi_reply_id` int(100) NOT NULL,
  `diskusi_reply_nama` varchar(100) NOT NULL,
  `diskusi_reply_isi` varchar(100) NOT NULL,
  `diskusi_reply_foto` varchar(100) NOT NULL,
  `id_diskusi` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_diskusi_reply`
--

INSERT INTO `tb_diskusi_reply` (`diskusi_reply_id`, `diskusi_reply_nama`, `diskusi_reply_isi`, `diskusi_reply_foto`, `id_diskusi`) VALUES
(1, 'Karolina Wilanda', 'coba sering makan daun bawang setiap hari semoga membantu :)', 'oline2.png', 10),
(2, 'Mohammad Ichwan Rizky Kurnia', 'minum madu', 'profile1.PNG', 11),
(3, 'Karolina Wilanda', 'kalo aku sih rajin olahraga aja', 'oline2.png', 11),
(4, 'ragilia panca kurniasa', 'makasih semuanyaaa !', 'caca.jpg', 11);

-- --------------------------------------------------------

--
-- Table structure for table `tb_listdokter`
--

CREATE TABLE IF NOT EXISTS `tb_listdokter` (
  `id_dokter` int(100) NOT NULL,
  `nama_dokter` varchar(100) NOT NULL,
  `jenis_dokter` varchar(100) NOT NULL,
  `pengalaman` varchar(100) NOT NULL,
  `pend_terakhir` varchar(100) NOT NULL,
  `tempat_bekerja` varchar(100) NOT NULL,
  `img_dokter` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_listdokter`
--

INSERT INTO `tb_listdokter` (`id_dokter`, `nama_dokter`, `jenis_dokter`, `pengalaman`, `pend_terakhir`, `tempat_bekerja`, `img_dokter`, `harga`) VALUES
(1, 'drg. Yely Matola', 'Dokter Gigi Umum', '9 tahun pengalaman', 'drg. - Kedokteran Gigi - Universitas Padjadjaran, 2009', 'D'' Dentist Dental Care, Jakarta', 'drg. Yely Matola.PNG', 0),
(2, 'Prof. Dr. dr. Teguh Santoso, M.D., Sp.PD-KKV, Sp.JP, Ph.D., FACC, FESC', 'Dokter Spesialis Penyakit Dalam', '43 tahun', 'Prof. - Professor - Universitas Indonesia, 1999', 'Rumah Sakit Medistra', '1.PNG', 0),
(3, 'drg. Datu Permata', 'Dokter Gigi Umum ', '9 tahun', 'drg. - Kedokteran Gigi - Universitas Trisakti, 2009', 'Klinik Nirmala Pondok Pinang', '2.PNG', 0),
(4, 'dr. Endang Triningsih, Sp.A(K)', 'Dokter Spesialis Anak', '29 tahun pengalaman', 'Sp.A - Spesialis Anak - Universitas Indonesia, 1995', 'Rumah Sakit Ibu dan Anak Harapan Kita', '3.PNG', 0),
(5, 'dr. Yunira Safitri, Sp.KK', 'Dokter Spesialis Kulit dan Kelamin', '5 tahun pengalaman', 'Sp.KK - Spesialis Kulit dan Kelamin - Universitas Indonesia, 2015', 'Rumah Sakit Ibu dan Anak Evasari', '4.PNG', 0),
(6, 'Dr. dr. Iwan Dakota, Sp.JP(K), M.A.R.S.', 'Dokter Spesialis Jantung dan Pembuluh Darah', '9 tahun pengalaman', 'Dr. - Doktor - Universitas Indonesia, 2014', 'Rumah Sakit Siloam Lippo Village', '5.PNG', 0),
(7, 'dr. Dimas Dwi Saputro, Sp.A', 'Dokter Spesialis Anak ', '5 tahun pengalaman', 'Sp.A - Spesialis Anak - Universitas Indonesia, 2015', 'Rumah Sakit Ibu dan Anak Harapan Kita', '6.PNG', 0),
(8, 'dr. Linda Evans Januar Ritanti', 'Dokter Umum', '11 tahun pengalaman', 'dr. - Kedokteran Umum - Universitas Kristen Indonesia', 'Klinik DK Bintaro', '7.PNG', 0),
(9, 'dr. Dimas Prasetyo Chandra', 'Dokter Umum', '7 tahun pengalaman', 'dr. - Kedokteran Umum - Universitas Pelita Harapan, 2011', 'Get Healthy Clinic', '8.PNG', 0),
(10, 'dr. Dewi Prabarini, Sp.OG', 'Dokter Spesialis Kebidanan dan Kandungan ', '9 tahun pengalaman', 'Sp.OG - Spesialis Obstetri & Ginekologi (Kebidanan dan Kandungan) - Universitas Indones', 'Rumah Sakit Ibu dan Anak Brawijaya', '9.PNG', 0),
(11, 'drg. Nadia Karalaini Hartana', 'Dokter Gigi Umum', '6 tahun pengalaman', 'drg. - Kedokteran Gigi - Universitas Trisakti, 2012', 'Praktek drg. Nadia Karalaini Hartana', '10.PNG', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tanya_dokter`
--

CREATE TABLE IF NOT EXISTS `tb_tanya_dokter` (
  `tanya_id` int(100) NOT NULL,
  `namadokter` varchar(100) NOT NULL,
  `namapenanya` varchar(100) NOT NULL,
  `isipertanyaan` varchar(100) NOT NULL,
  `jawabanpertanyaan` varchar(100) NOT NULL,
  `fotodokter` varchar(100) NOT NULL,
  `fotopenanya` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tanya_dokter`
--

INSERT INTO `tb_tanya_dokter` (`tanya_id`, `namadokter`, `namapenanya`, `isipertanyaan`, `jawabanpertanyaan`, `fotodokter`, `fotopenanya`) VALUES
(30, 'drg. Datu Permata', 'usertes', 'zzzz', 'Pertanyaan Anda Belum Dijawab', '2.PNG', 'profile1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tipskesehatan`
--

CREATE TABLE IF NOT EXISTS `tb_tipskesehatan` (
  `tips_id` int(100) NOT NULL,
  `tips_judul` varchar(100) NOT NULL,
  `tips_isi` longtext NOT NULL,
  `tips_tanggal` varchar(100) NOT NULL,
  `tips_gambar` varchar(100) NOT NULL,
  `tips_category` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tipskesehatan`
--

INSERT INTO `tb_tipskesehatan` (`tips_id`, `tips_judul`, `tips_isi`, `tips_tanggal`, `tips_gambar`, `tips_category`) VALUES
(1, 'Studi: Pangkas Kalori 15 Persen Bikin Awet Muda!', 'Ngomongin kalori, kaitannya nggak cuma dengan berat badan. Menurut penelitian terbaru, memangkas asupan kalori hingga 15 persen selama dua tahun bisa memperlambat proses penuaan.\n\nStudi yang dipublikasikan di Journal Cell Metabolism tersebut mengatakan bahwa asupan kalori berpengaruh pula pada berbagai risiko penyakit yang terkait metabolisme. Termasuk di antaranya diabetes, kanker, dan alzheimer yang merupakan salah satu jenis pikun.\n', '2018-12-06T05:34:39.282Z', 'diet.png', 'diet'),
(2, 'Rahasia Bugar Taron Egerton, Pemeran Robin Hood yang Kekar', 'Setelah sukses berlaga dalam film Kingsman: The Secret Service, aktor Taron Egerton kembali didapuk jadi pemeran utama dalam film Robin Hood yang rilis kemarin Rabu, (21/11/2018). Taron juga dikenal sebagai salah satu aktor bertubuh bugar dan kekar, yang ia dapatkan dari rutin berolahraga.\r\n\r\nApa rahasianya? Dalton Wong dari Twenty Two Training Opens a New Window yang melatihnya, mengatakan bahwa olahraga Taron berfokus pada kardio berintensitas tinggi, olahraga kekuatan multi-disiplin, dan latihan fleksibilitas. Ia tak melulu memfokuskan Taron untuk mengangkat beban untuk membentuk otot seperti kebanyakan para aktor.', '2018-12-06T05:34:39.282Z', 'kebugaran.png', 'kebugaran'),
(3, 'Manfaat Sehat Kakap, Jenis Ikan yang Dimasak Susi Pudjiastuti', 'Menteri Kelautan dan Perikanan Indonesia Susi Pudjiastuti menunjukkan kebolehannya mengolah kakap pada Minggu (25/11/2018). Susi memilih menu kakap pindang gunung yang merupakan menu khas Pangandaran, Jawa Barat.\r\n\r\nKakap atau snapper adalah salah satu jenis ikan yang kaya kandungan nutrisi. Dikutip dari Healthy Eating SFGate, kakap mengandung rendah kalori dan tinggi protein. Kakap juga kaya selenium, vitamin A, potasium, dan asam lemak omega 3. Konsumsi ikan kakap membantu melengkapi kecukupan nutrisi tiap hari.', '2018-12-06T05:34:39.282Z', '3.PNG', 'diet'),
(4, 'Mau Atur Jadwal Makan dan Olahraga? Sore, Tubuh Bakar Lebih Banyak Kalori', 'Kapan waktu terbaik untuk olahraga? Jawabannya akan sangat tergantung dari situasi tiap orang namun secara umum sebaiknya sesuaikan dengan jam biologis tubuh. Studi terbaru yang dipublikasi di jurnal Current Biology melihat ternyata tubuh kita bisa membakar lebih banyak kalori di jam-jam tertentu.\r\n\r\nPemimpin studi Kirsi-Marja Zitting dari Division of Sleep and Circadian Disorders Brigham and Women''s Hospital mengatakan bahwa tubuh rata-rata akan membakar 10 persen kalori lebih banyak waktu siang hingga sore hari. Hal ini diketahui setelah peneliti melakukan eksperimen pada tujuh orang', '2018-12-06T05:34:39.282Z', '4.PNG', 'diet'),
(5, 'Ini Lho, Diet Supaya Bebas Jerawat', 'Untuk menghindari jerawat sebaiknya batasi asupan kafein, coklat, alkohol, mentega hewani (butter), keju, dan hidangan laut. Asupan lain yang sebaiknya dihindari adalah makanan olahan dan cepat saji, yang mengandung banyak gula, garam, serta lemak.\r\n\r\nSelain itu, pastikan minum air putih 8 gelas per hari atau dua liter untuk menjaga kesehatan kulit. Buah dan sayur juga harus dikonsumsi setiap hari untuk menjaga kulit selalu terlihat lembut dan bercahaya. Buah, sayur, dan air putih adalah agen penyembuh bagi kulit yang terpapar efek diet yang buruk', '2018-12-06T05:34:39.282Z', '5.PNG', 'diet'),
(6, 'Kepo Diet Kemal Mochtar? Ini Nih Penjelasan dari Orangnya Langsung', 'Banyak yang penasaran dengan diet yang dilakukan oleh penyiar radio dan presenter, Kemal Mochtar. Bayangkan saja nih, setelah berbobot 120 kg, akhirnya Kemal bisa memangkas berat badannya menjadi 63 kg. Wah drastis banget kan, ya!\r\n\r\nini dia tips yang dilakukan kemal ,mengatur pola makannya, Kemal juga membiasakan diri untuk berjalan kaki setiap hari selama 30-60 menit. Hasilnya? Kalian bisa lihat sendiri, Kemal kini punya berat badan yang ideal. Selain itu dia juga jadi jarang sakit.', '2018-12-06T05:34:39.282Z', '6.PNG', 'diet'),
(7, 'Mau Tahu Penyebab Gemuk? Itu Tergantung di Bagian Mana Lemak Menumpuk', 'Ada banyak sekali penyebab angka di timbangan melonjak tidak terkontrol. Mulai dari porsi makan yang berlebihan, hingga aktivitas hormon tertentu yang memperlambat metabolisme.\r\n\r\nFaktor usia juga membuat metabolisme cenderung melambat, sehingga mudah terjadi timbunan lemak di tubuh. Demikian juga stres, yang secara tidak langsung mendorong perilaku makan berlebih, yang juga penyebab gemuk.\r\n', '2018-12-06T05:34:39.282Z', '7.PNG', 'diet'),
(8, '''Flexitarian'', Tren Diet Bagi yang Nggak Sanggup Jadi Vegetarian', 'Dikutip dari Health Line, flexitarian adalah gaya makan yang mendorong makanan nabati, namun masih mengonsumsi produk hewani seperti daging dalam jumlah sedang.\r\n\r\n\r\nDiet flexitarian tidak memiliki aturan yang jelas atau jumlah kalori dan makronutrien yang disarankan. Mereka yang menerapkannya berpegang pada"\r\n\r\n- Makan sebagian besar buah-buahan, sayuran, kacang-kacangan dan biji-bijian.\r\n- Fokus pada protein dari tumbuhan sebagai ganti protein hewan.\r\n- Jadilah fleksibel dan gabungkan produk hewani.\r\n- Kurangi makanan yang diproses, perbanyak makanan alami.\r\n- Batasi tambahan gula dan pemanis.', '2018-12-06T05:34:39.282Z', '8.png', 'diet'),
(9, 'Mengenal Fisioterapi, Solusi untuk Cedera dan Nyeri Sendi', ' Dalam melakukan segala aktivitas keseharian, seringkali kita tak menyadari bahwa postur tubuh yang salah dalam melakukan sesuatu seperti membungkuk dan mengangkat beban, ternyata bila dilakukan terus menerus, menjadi kebiasaan, dan terakumulasi ternyata dapat menimbulkan dampak yang buruk, yaitu gangguan fungsional pada otot tubuh.\r\n\r\n"Gangguan gejala yang biasa dialaminya ini biasanya itu otot kaku dan nyeri sendi, hal ini bisa diatasi dengan fisioterapi,"', '2018-12-06T05:34:39.282Z', 'kebugaran2.png', 'kebugaran'),
(10, 'Duduk dan Rokok Sama-Sama Mematikan, Apa Kabar Duduk-Duduk Sambil Merokok?', 'Duduk seolah menjadi bagian dari kehidupan masyarakat pada saat ini. Daripada berdiri lama bikin pegal, tentu saja sebagian masyarakat akan memilih untuk duduk ketimbang berdiri. Terutama untuk para pekerja kantoran, seolah duduk lebih dari 8 jam dalam sehari menjadi hal yang lumrah. \r\n\r\nNamun perlu diketahui, ternyata duduk terlalu lama memiliki dampak yang mematikan lho. \r\n\r\nSaat ini muncul istilah ''sitting is the new smoking''. Ternyata duduk dapat menjadi penyebab seseorang meninggal dunia. Tentu fenomena duduk lama ini perlu diwaspadai. ', '2018-12-06T05:34:39.282Z', 'kebugaran3.png', 'kebugaran'),
(11, 'Banyak Duduk Bisa Mematikan, Bagaimana Nasib Pekerja Kantoran?', 'Hal yang belum banyak diketahui masyarakat saat ini adalah bahaya terlalu banyak duduk. Duduk yang dianggap seolah menjadi hal yang begitu sepele, ternyata dapat menjadi penyebab kematian.\r\n\r\nBahkan saat ini sudah ada istilah sitting is the new smoking, yang menyatakan bahwa efek dari duduk yang terlalu lama ternyata sama berbahayanya dengan merokok.\r\n\r\nLalu bagaimana nasib pekerja yang mengharuskan duduk dengan durasi yang cukup lama setiap harinya? \r\n\r\n"Apabila di kantor, setiap 3 jam sekali usahakan lalukan kegiatan fisik, yang ringan-ringan saja, agar otot-ototnya tidak kaku," jelas founder dan senior fisioterapis EvoPhysio.id, S. Rujito', '2018-12-06T05:34:39.282Z', 'kebugaran4.png', 'kebugaran'),
(12, 'Detak Jantung Melambat Saat Meditasi, Apa yang Sebenarnya Terjadi?', ' Bos dan CEO Twitter Jack Dorsey menyebut denyut jantungnya melambat hingga di bawah 40 bpm (beats per minute) saat melakukan meditasi Vipassana di Myanmar baru-baru ini. Data ini terpantau oleh sensor di Apple Watch yang dikenakannya.\r\n\r\nDetak jantung yang melambat banyak dikaitkan dengan pikiran yang lebih tenang alias rileks. Dalam kondisi sebaliknya yakni saat stres, detak atau denyut jantung biasanya meningkat.\r\n\r\n"Detak jantung melambat saat meditasi dapat terjadi karena sistem parasimpatis saraf kita meningkat, sehingga dapat menurunkan tekanan darah, nadi, memperbaiki efektivitas pernafasan, dll," kata dr Ayuthia Putri Sedyawan, BMedSc, SpJP, FIHA', '2018-12-06T05:34:39.282Z', 'kebugaran5.png', 'kebugaran'),
(13, 'Tips Bugar untuk Pekerja Kantoran: Manfaatkan Jam Makan Siang!', 'Kebanyakan pekerja kantoran mengeluhkan waktunya yang padat menjadikan mereka sulit untuk berolahraga seperti mengunjungi pusat kebugaran atau sekadar jogging di pagi hari. Padahal, ada beberapa waktu luang yang bisa digunakan untuk melakukan aktivitas fisik untuk membiasakan pekerja bergerak agar terhindar dari berbagai macam penyakit.\r\n\r\nJam makan siang bisa menjadi pilihan untuk berolahraga santai. Misal berjalan atau melakukan gerakan sederhana lainnya.', '2018-12-06T05:34:39.282Z', 'kebugaran6.png', 'kebugaran');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `user_id` int(100) NOT NULL,
  `user_nama` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_img` varchar(100) NOT NULL,
  `user_hakakses` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `user_nama`, `user_email`, `user_password`, `user_img`, `user_hakakses`) VALUES
(8, 'usertes', '1', '1', '1.PNG', 'user'),
(1, 'Mohammad Ichwan Rizky Kurnia', 'ichwanrizky123@gmail.com', 'sarutobi11', 'profile1.PNG', 'user'),
(2, 'Karolina Wilanda', 'oline123@gmail.com', 'oline123', 'oline2.png', 'user'),
(10, 'ragilia panca kurniasa', 'ragiliapanca5@gmail.com', 'ragilia123', 'caca.jpg', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  ADD PRIMARY KEY (`artikel_id`);

--
-- Indexes for table `tb_diskusi`
--
ALTER TABLE `tb_diskusi`
  ADD PRIMARY KEY (`id_diskusi`), ADD UNIQUE KEY `id_diskusi` (`id_diskusi`), ADD KEY `nama` (`nama`), ADD KEY `foto` (`foto`), ADD KEY `email` (`email`);

--
-- Indexes for table `tb_diskusi_reply`
--
ALTER TABLE `tb_diskusi_reply`
  ADD PRIMARY KEY (`diskusi_reply_id`), ADD UNIQUE KEY `diskusi_reply_id` (`diskusi_reply_id`), ADD KEY `diskusi_reply_nama` (`diskusi_reply_nama`), ADD KEY `diskusi_reply_foto` (`diskusi_reply_foto`), ADD KEY `id_diskusi` (`id_diskusi`);

--
-- Indexes for table `tb_listdokter`
--
ALTER TABLE `tb_listdokter`
  ADD PRIMARY KEY (`id_dokter`), ADD KEY `nama_dokter` (`nama_dokter`), ADD KEY `img_dokter` (`img_dokter`);

--
-- Indexes for table `tb_tanya_dokter`
--
ALTER TABLE `tb_tanya_dokter`
  ADD PRIMARY KEY (`tanya_id`), ADD KEY `namadokter` (`namadokter`), ADD KEY `namapenanya` (`namapenanya`), ADD KEY `fotodokter` (`fotodokter`), ADD KEY `fotopenanya` (`fotopenanya`);

--
-- Indexes for table `tb_tipskesehatan`
--
ALTER TABLE `tb_tipskesehatan`
  ADD PRIMARY KEY (`tips_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`user_email`), ADD KEY `user_id` (`user_id`), ADD KEY `user_email` (`user_email`), ADD KEY `user_img` (`user_img`), ADD KEY `user_nama` (`user_nama`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  MODIFY `artikel_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_diskusi`
--
ALTER TABLE `tb_diskusi`
  MODIFY `id_diskusi` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_diskusi_reply`
--
ALTER TABLE `tb_diskusi_reply`
  MODIFY `diskusi_reply_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_listdokter`
--
ALTER TABLE `tb_listdokter`
  MODIFY `id_dokter` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_tanya_dokter`
--
ALTER TABLE `tb_tanya_dokter`
  MODIFY `tanya_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tb_tipskesehatan`
--
ALTER TABLE `tb_tipskesehatan`
  MODIFY `tips_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_diskusi`
--
ALTER TABLE `tb_diskusi`
ADD CONSTRAINT `diskusi_email` FOREIGN KEY (`email`) REFERENCES `tb_user` (`user_email`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `diskusi_img` FOREIGN KEY (`foto`) REFERENCES `tb_user` (`user_img`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `diskusi_nama` FOREIGN KEY (`nama`) REFERENCES `tb_user` (`user_nama`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_diskusi_reply`
--
ALTER TABLE `tb_diskusi_reply`
ADD CONSTRAINT `reply_diskusi` FOREIGN KEY (`id_diskusi`) REFERENCES `tb_diskusi` (`id_diskusi`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `reply_foto` FOREIGN KEY (`diskusi_reply_foto`) REFERENCES `tb_user` (`user_img`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `reply_nama` FOREIGN KEY (`diskusi_reply_nama`) REFERENCES `tb_user` (`user_nama`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_tanya_dokter`
--
ALTER TABLE `tb_tanya_dokter`
ADD CONSTRAINT `tanya_fotodokter` FOREIGN KEY (`fotodokter`) REFERENCES `tb_listdokter` (`img_dokter`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tanya_fotopenanya` FOREIGN KEY (`fotopenanya`) REFERENCES `tb_user` (`user_img`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tanya_namadokter` FOREIGN KEY (`namadokter`) REFERENCES `tb_listdokter` (`nama_dokter`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tanya_namapenanya` FOREIGN KEY (`namapenanya`) REFERENCES `tb_user` (`user_nama`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
