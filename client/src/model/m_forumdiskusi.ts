export class ForumDiskusi {
    constructor(public 	id_diskusi: string = "",
                public 	nama: string = "",
                public 	email: string = "",
                public 	tanggal: string = "",
                public 	diskusi: string = "",
                public 	foto: string = "",
                public open: any) {                    
                }
}