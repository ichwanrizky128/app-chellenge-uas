export class listdokter {
    constructor(public 	id_dokter: string = "",
                public 	nama_dokter:"",
                public 	jenis_dokter: string = "",
                public 	pengalaman: string = "",
                public 	pend_terakhir: string = "",
                public 	tempat_bekerja: string = "",
                public 	img_dokter: string = "",
                public open: any) {                    
                }
}