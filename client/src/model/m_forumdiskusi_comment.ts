export class forumdiskusi_comment {
    constructor(public 	diskusi_reply_id: string = "",
                public 	diskusi_reply_nama: string = "",
                public 	diskusi_reply_isi: string = "",
                public 	diskusi_reply_foto: string = "",
                public 	id_diskusi: string = "") {                    
                }
}