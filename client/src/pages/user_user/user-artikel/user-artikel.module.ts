import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserArtikelPage } from './user-artikel';

@NgModule({
  declarations: [
    UserArtikelPage,
  ],
  imports: [
    IonicPageModule.forChild(UserArtikelPage),
  ],
})
export class UserArtikelPageModule {}
