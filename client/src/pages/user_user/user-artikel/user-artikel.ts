import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PArtikelProvider } from '../../../providers/p-artikel/p-artikel';
import { artikelKesehatan } from '../../../model/m_artikel';
import { UserArtikelDetailPage } from '../user-artikel-detail/user-artikel-detail';

@IonicPage()
@Component({
  selector: 'page-user-artikel',
  templateUrl: 'user-artikel.html',
})
export class UserArtikelPage {
  public artikel: artikelKesehatan[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public artikelKesehatan: PArtikelProvider) {
  }

  ngOnInit(){
    this.getArtikel();
   }
 
   getArtikel(){
     this.artikel = [];
     this.artikelKesehatan.getArtikel().subscribe((result) => {
       console.log(result);
       this.artikel = result;
     })
   }
 
   artikelDetail(item){
     this.navCtrl.push(UserArtikelDetailPage, item)
   }

}
