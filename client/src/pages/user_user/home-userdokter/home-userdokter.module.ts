import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeUserdokterPage } from './home-userdokter';

@NgModule({
  declarations: [
    HomeUserdokterPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeUserdokterPage),
  ],
})
export class HomeUserdokterPageModule {}
