import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { PTanyadokterCekProvider } from '../../../providers/p-tanyadokter-cek/p-tanyadokter-cek';
import { tanyadokter } from '../../../model/m_tanyadokter_cek';
import { PUserProvider } from '../../../providers/p-user/p-user';
import { HomeUserPage } from '../home-user/home-user';

@IonicPage()
@Component({
  selector: 'page-user-tanyadokter-request',
  templateUrl: 'user-tanyadokter-request.html',
})
export class UserTanyadokterRequestPage {
  items: any;
  private isiTanya: string = "";

  constructor(public alertCtrl: AlertController, public loadingCtrl: LoadingController, public userProvider: PUserProvider, public tanyadokterProvider: PTanyadokterCekProvider, public navCtrl: NavController, public navParams: NavParams) {
    console.log(this.navParams.data);
    this.items = this.navParams.data;
  }

  tanya(){
    var data = this.navParams.data;
    this.userProvider.getUser3(data.user_id).subscribe((result) => {
      for (let item of result){
        var data2 = {
          "tanya_id": "",
          "namadokter": data.nama_dokter,
          "namapenanya": item.user_nama,
          "isipertanyaan": this.isiTanya,
          "jawabanpertanyaan": "",
          "fotodokter": data.img_dokter,
          "fotopenanya": item.user_img,
        }
        this.tanyadokterProvider.addTanyadokter(data2).subscribe((result) => {
          console.log(result);
          var data3 = {
            "user_id": item.user_id,
            "user_img": item.user_img
          }
          const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 3000
          });
          loader.present();
              setTimeout(() => {
                const alert = this.alertCtrl.create({
                  subTitle: 'Pertanyaan Berhasil Dikirim Silahkan Cek Di Profile Page',
                  buttons: ['OK']
                 });
                 alert.present();
              this.navCtrl.setRoot(HomeUserPage, data3);
            },3000);         
        });
      }  
    });   
  }

}
