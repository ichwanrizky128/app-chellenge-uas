import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserArtikelDetailPage } from './user-artikel-detail';

@NgModule({
  declarations: [
    UserArtikelDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(UserArtikelDetailPage),
  ],
})
export class UserArtikelDetailPageModule {}
