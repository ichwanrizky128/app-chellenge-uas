import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserTanyadokterPage } from './user-tanyadokter';

@NgModule({
  declarations: [
    UserTanyadokterPage,
  ],
  imports: [
    IonicPageModule.forChild(UserTanyadokterPage),
  ],
})
export class UserTanyadokterPageModule {}
