import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { listdokter } from '../../../model/m_tanyadokter';
import { PTanyadokterProvider } from '../../../providers/p-tanyadokter/p-tanyadokter';
import { UserTanyadokterRequestPage } from '../user-tanyadokter-request/user-tanyadokter-request';

@IonicPage()
@Component({
  selector: 'page-user-tanyadokter',
  templateUrl: 'user-tanyadokter.html',
})
export class UserTanyadokterPage {
  public list_dokter: listdokter[] = [];

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public tanyadokterProvider: PTanyadokterProvider) {
  }

  ngOnInit(){
    this.getListdokter();
  }

  getListdokter(){
    this.list_dokter = [];
    this.tanyadokterProvider.getListdokter().subscribe((result) => {
      console.log(result);
      this.list_dokter = result;
    });
    console.log(this.list_dokter);
  }


  toggleSection(i){
    this.list_dokter[i].open = !this.list_dokter[i].open;
  }

  getItems(ev){
    let val = ev.target.value;
    if (!val || !val.trim()){
      this.getListdokter();
    }
    else{
      this.list_dokter = [];
      this.tanyadokterProvider.cariDokter(val).subscribe((result) => {
        console.log(result);
        this.list_dokter = result;
      });
    }
  }

  requestDoct(item){
    var data = {
      "id_dokter":item.id_dokter,
      "nama_dokter":item.nama_dokter,
      "jenis_dokter":item.jenis_dokter,
      "pengalaman":item.pengalaman,
      "pend_terakhir":item.pend_terakhir,
      "tempat_bekerja":item.tempat_bekerja,
      "img_dokter":item.img_dokter,
      "user_id":this.navParams.data
    }
    let modal = this.modalCtrl.create(UserTanyadokterRequestPage, data);
    modal.present();
  }

}
