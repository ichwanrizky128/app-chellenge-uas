import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserTipskesehatanPage } from './user-tipskesehatan';

@NgModule({
  declarations: [
    UserTipskesehatanPage,
  ],
  imports: [
    IonicPageModule.forChild(UserTipskesehatanPage),
  ],
})
export class UserTipskesehatanPageModule {}
