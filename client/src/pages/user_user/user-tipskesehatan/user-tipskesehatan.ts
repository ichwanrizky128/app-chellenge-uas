import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { tipskesehatan } from '../../../model/m_tipskesehatan';
import { PTipskesehatanProvider } from '../../../providers/p-tipskesehatan/p-tipskesehatan';
import { UserTipskesehatanDetailPage } from '../user-tipskesehatan-detail/user-tipskesehatan-detail';

@IonicPage()
@Component({
  selector: 'page-user-tipskesehatan',
  templateUrl: 'user-tipskesehatan.html',
})
export class UserTipskesehatanPage {
  public tips: tipskesehatan[] = [];
  categorySelected = "diet";

  constructor(public navCtrl: NavController, public navParams: NavParams, public tipsKesehatan: PTipskesehatanProvider) {
  }

  ngOnInit(){
    this.getTips();
  }

  getTips(){
    this.tips = [];
    this.tipsKesehatan.getTips(this.categorySelected).subscribe((result) => {
      console.log(result);
      this.tips = result;
    })
  }

  tipsDetail(item){
    this.navCtrl.push(UserTipskesehatanDetailPage, item);
  }

}
