import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfileChangepasswordPage } from './user-profile-changepassword';

@NgModule({
  declarations: [
    UserProfileChangepasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfileChangepasswordPage),
  ],
})
export class UserProfileChangepasswordPageModule {}
