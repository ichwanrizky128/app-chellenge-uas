import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PUserProvider } from '../../../providers/p-user/p-user';

@IonicPage()
@Component({
  selector: 'page-user-profile-changepassword',
  templateUrl: 'user-profile-changepassword.html',
})
export class UserProfileChangepasswordPage {
  private user_id: string = "";
  private user_oldpassword: string = "";
  private user_password: string = "";
  private user_newpassword:string = "";
  private user_confirmpassword:string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams, public updatePasswordProvider: PUserProvider, public alertCtrl: AlertController) {
    var data = this.navParams.data
    this.user_id = data.user_id;
    this.user_password = data.user_password;
  }

  close(){
    this.navCtrl.pop();
  }

  updatePassword(){
    if (this.user_oldpassword == ""){
      const alert = this.alertCtrl.create({
        subTitle: 'Password Tidak Boleh Kosong !',
        buttons: ['OK']
       });
       alert.present();
    }

    else if (this.user_newpassword == ""){
      const alert = this.alertCtrl.create({
        subTitle: 'Password Tidak Boleh Kosong !',
        buttons: ['OK']
       });
       alert.present();
    }

    else if (this.user_confirmpassword == ""){
      const alert = this.alertCtrl.create({
        subTitle: 'Password Tidak Boleh Kosong !',
        buttons: ['OK']
       });
       alert.present();
    }

    else if(this.user_oldpassword != this.user_password){
      const alert = this.alertCtrl.create({
        subTitle: 'Password Lama Salah !',
        buttons: ['OK']
       });
       alert.present();
    }

    else if (this.user_oldpassword == this.user_newpassword){
      const alert = this.alertCtrl.create({
        subTitle: 'Password Baru Sama Dengan Password Lama !',
        buttons: ['OK']
       });
       alert.present();
    }

    else if (this.user_newpassword != this.user_confirmpassword){
      const alert = this.alertCtrl.create({
        subTitle: 'Password Tidak Sama !',
        buttons: ['OK']
       });
       alert.present();
    }

    else if (this.user_oldpassword != "this.user_confirmpassword"){
      const alert = this.alertCtrl.create({
        subTitle: 'Password Tidak Sama !',
        buttons: ['OK']
       });
       alert.present();
    }

    else{
      var data = {
        "user_id": this.user_id,
        "user_password": this.user_newpassword
      }
      this.updatePasswordProvider.updatePassword(this.user_id, data).subscribe((result) =>{
        console.log(result);
        this.navCtrl.pop();
      });
    }
  }


}
