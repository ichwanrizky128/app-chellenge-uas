import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfileHistorypostPage } from './user-profile-historypost';

@NgModule({
  declarations: [
    UserProfileHistorypostPage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfileHistorypostPage),
  ],
})
export class UserProfileHistorypostPageModule {}
