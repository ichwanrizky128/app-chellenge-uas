import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ForumDiskusi } from '../../../model/m_forumdiskusi';
import { PForumdiskusiProvider } from '../../../providers/p-forumdiskusi/p-forumdiskusi'; 
import { UserForumdiskusiCommentPage } from '../user-forumdiskusi-comment/user-forumdiskusi-comment';
import { PUserProvider } from '../../../providers/p-user/p-user';
import { User } from '../../../model/m_user';


@IonicPage()
@Component({
  selector: 'page-user-forumdiskusi',
  templateUrl: 'user-forumdiskusi.html',
})
export class UserForumdiskusiPage {
  public forumdiskusi: ForumDiskusi[] = [];
  public user: User[]= [];
  private postingan: string = "";
  tanggal: any = new Date();

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, public forumDiskusi: PForumdiskusiProvider, public userProvider: PUserProvider) {
  }

  ngOnInit(){
    this.getDiskusi();
  }

  getDiskusi(){
    this.forumdiskusi = [];
    this.forumDiskusi.getDiskusi().subscribe((result) => {
      console.log(result);
      this.forumdiskusi = result;
    });
  }

  addDiskusi(){
    this.user = [];
    this.userProvider.getUser3(this.navParams.data).subscribe((result) => {
      console.log(result);
      for(let item of result){
        var data = {
          "id_diskusi": "",
          "nama": item.user_nama,
          "email": item.user_email,
          "tanggal": this.tanggal,
          "diskusi": this.postingan,
          "foto": item.user_img,
        }
        console.log(data);
        this.forumDiskusi.addDiskusi(data).subscribe((result) => {
          console.log(result);
          this.getDiskusi();
        });
      }
    });
  }

  viewComment(item){
    this.user = [];
    this.userProvider.getUser3(this.navParams.data).subscribe((result) => {
      for(let data of result){
        var data2 = {
          "id_diskusi": item.id_diskusi,
          "nama": data.user_nama,
          "foto": data.user_img,
        }
    
        let modal = this.modalCtrl.create(UserForumdiskusiCommentPage, data2);
        modal.present();
      } 
    });   
  }

}
