import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserForumdiskusiPage } from './user-forumdiskusi';

@NgModule({
  declarations: [
    UserForumdiskusiPage,
  ],
  imports: [
    IonicPageModule.forChild(UserForumdiskusiPage),
  ],
})
export class UserForumdiskusiPageModule {}
