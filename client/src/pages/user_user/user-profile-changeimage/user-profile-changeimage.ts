import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PUserProvider } from '../../../providers/p-user/p-user';

@IonicPage()
@Component({
  selector: 'page-user-profile-changeimage',
  templateUrl: 'user-profile-changeimage.html',
})
export class UserProfileChangeimagePage {
  private user_id: string = "";
  private user_img: string = "";
  private user_newimg: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public updateImgProvider: PUserProvider, public alertCtrl: AlertController) {
    var data = this.navParams.data
    this.user_id = data.user_id;
    this.user_img = data.user_img;
  }

  close(){
    this.navCtrl.pop();
  }

  updateImg(){
    if (this.user_newimg == ""){
      const alert = this.alertCtrl.create({
        subTitle: 'Image Tidak Boleh Kosong !',
        buttons: ['OK']
       });
       alert.present();
    }

    else {
      var path = this.user_newimg;
      var filename = path.replace(/^C:\\fakepath\\/, "");
      var data = {
        "user_id": this.user_id,
        "user_img": filename
      }
      this.updateImgProvider.updateImg(this.user_id, data).subscribe((result) =>{
        console.log(result);
        this.navCtrl.pop();
      })
    }
  }
  

}
