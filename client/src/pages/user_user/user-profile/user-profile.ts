import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { User } from '../../../model/m_user';
import { PUserProvider } from '../../../providers/p-user/p-user';
import { UserProfileEditPage } from '../user-profile-edit/user-profile-edit';
import { UserProfileChangepasswordPage } from '../user-profile-changepassword/user-profile-changepassword';
import { UserProfileChangeimagePage } from '../user-profile-changeimage/user-profile-changeimage';
import { UserProfileHistorypostPage } from '../user-profile-historypost/user-profile-historypost';
import { HomePage } from '../../user_no/home/home';
import { UserTanyadokterCekPage } from '../user-tanyadokter-cek/user-tanyadokter-cek';


@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {
  public user: any;

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, public profile: PUserProvider) {
  }

  ngOnInit(){
    this.getProfile();
  }

  getProfile(){
    this.profile.getUser3(this.navParams.data).subscribe((result) => {
      console.log(result);
      this.user = result;
    });   
  }

  editProfile(){
    this.profile.getUser3(this.navParams.data).subscribe((result) => {
      for(let item of result){
        var data = {
          "user_id": item.user_id,
          "user_nama": item.user_nama,
          "user_email": item.user_email,
          "user_img": item.user_img
        }

        var modal = this.modalCtrl.create(UserProfileEditPage, data);
        modal.onDidDismiss(() => {
          this.getProfile();
        })
        modal.present();
      }
    });
  }

  changePassword(){
    this.profile.getUser3(this.navParams.data).subscribe((result) => {
      for(let item of result){
        var data = {
          "user_id": item.user_id,
          "user_password": item.user_password,
        }

        var modal = this.modalCtrl.create(UserProfileChangepasswordPage, data);
        modal.onDidDismiss(() => {
          this.getProfile();
        })
        modal.present();
      }
    });
  }

  changeImage(){
    this.profile.getUser3(this.navParams.data).subscribe((result) => {
      for(let item of result){
        var data = {
          "user_id": item.user_id,
          "user_img": item.user_img,
        }

        var modal = this.modalCtrl.create(UserProfileChangeimagePage, data);
        modal.onDidDismiss(() => {
          this.getProfile();
        })
        modal.present();
      }
    });
  }

  historyPost(){
    this.profile.getUser3(this.navParams.data).subscribe((result) => {
      for(let item of result){
        var data = {
          "user_email": item.user_email,
          "user_id": item.user_id,
        }

        var modal = this.modalCtrl.create(UserProfileHistorypostPage, data);
        modal.onDidDismiss(() => {
          this.getProfile();
        })
        modal.present();
      }
    });
  }

  tanyaDokter(){
    this.profile.getUser3(this.navParams.data).subscribe((result) => {
      for(let item of result){
        var data = {
          "user_nama": item.user_nama,
          "user_id": item.user_id,
          "user_img": item.user_img
        }

        var modal = this.modalCtrl.create(UserTanyadokterCekPage, data);
        modal.onDidDismiss(() => {
          this.getProfile();
        })
        modal.present();
      }
    }); 
  }

  logOut(){
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
    setTimeout(() => {
      this.navCtrl.setRoot(HomePage);      
    },3000);
    
  }

}
