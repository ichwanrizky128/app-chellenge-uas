import { Component } from '@angular/core';
import { NavController, NavParams, Events  } from 'ionic-angular';
import { tipskesehatan } from '../../../model/m_tipskesehatan';
import { PTipskesehatanProvider } from '../../../providers/p-tipskesehatan/p-tipskesehatan';
import { UserForumdiskusiPage } from '../user-forumdiskusi/user-forumdiskusi';
import { UserTanyadokterPage } from '../user-tanyadokter/user-tanyadokter';
import { UserTipskesehatanDetailPage } from '../user-tipskesehatan-detail/user-tipskesehatan-detail';
import { UserProfilePage } from '../user-profile/user-profile';
import { UserTipskesehatanPage } from '../user-tipskesehatan/user-tipskesehatan';
import { UserArtikelPage } from '../user-artikel/user-artikel';

@Component({
  selector: 'page-home-user',
  templateUrl: 'home-user.html',
})
export class HomeUserPage {
  public tips: tipskesehatan[] = [];
  categorySelected = "diet";
  private id: string = "";
  private userimg: string = "";

  constructor(public events:Events, public navCtrl: NavController, public navParams: NavParams, public tipsKesehatan: PTipskesehatanProvider) {
    console.log(this.navParams.data);
    var data = this.navParams.data;
    this.id = data.user_id;
    this.userimg = data.user_img;
  }

  ngOnInit(){
    this.getTips();
  }

  getTips(){
    this.tips = [];
    this.tipsKesehatan.getTips(this.categorySelected).subscribe((result) => {
      console.log(result);
      this.tips = result;
    });
  }

  user_artikel(){
    this.navCtrl.push(UserArtikelPage);
  }

  user_forumDiskusi(){
    this.navCtrl.push(UserForumdiskusiPage, this.id);
  }

  user_tanyadokter(){
    this.navCtrl.push(UserTanyadokterPage, this.id);
  }

  user_tipskesehatan(){
    this.navCtrl.push(UserTipskesehatanPage);
  }

  tipsDetail(item){
    this.navCtrl.push(UserTipskesehatanDetailPage, item);
  }

  profile(){
    this.navCtrl.push(UserProfilePage, this.id);
  }

}
