import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoForumdiskusiCommentPage } from './no-forumdiskusi-comment';

@NgModule({
  declarations: [
    NoForumdiskusiCommentPage,
  ],
  imports: [
    IonicPageModule.forChild(NoForumdiskusiCommentPage),
  ],
})
export class NoForumdiskusiCommentPageModule {}
