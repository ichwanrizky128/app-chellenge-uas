import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NoForumdiskusiPage } from '../no-forumdiskusi/no-forumdiskusi';
import { NoTanyadokterPage } from '../no-tanyadokter/no-tanyadokter';
import { LoginPage } from '../login/login';
import { NoTipskesehatanPage } from '../no-tipskesehatan/no-tipskesehatan';

import { tipskesehatan } from '../../../model/m_tipskesehatan';
import { PTipskesehatanProvider } from '../../../providers/p-tipskesehatan/p-tipskesehatan';
import { NoTipskesehatanDetailPage } from '../no-tipskesehatan-detail/no-tipskesehatan-detail';
import { NoArtikelPage } from '../no-artikel/no-artikel';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public tips: tipskesehatan[] = [];
  categorySelected = "diet";

  private count1: any;
  private count2: any;
  private hasil:any;
  constructor(public navCtrl: NavController, public tipsKesehatan: PTipskesehatanProvider) {

  }

  ngOnInit(){
    this.getTips();
  }

  getTips(){
    this.tips = [];
    this.tipsKesehatan.getTips(this.categorySelected).subscribe((result) => {
      console.log(result);
      this.tips = result;
    });
  }

  no_artikel(){
    this.navCtrl.push(NoArtikelPage);
  }

  no_forumDiskusi(){
    this.navCtrl.push(NoForumdiskusiPage);
  }

  no_tanyadokter(){
    this.navCtrl.push(NoTanyadokterPage);
  }

  login(){
    this.navCtrl.push(LoginPage);
  }

  no_tipskesehatan(){
    this.navCtrl.push(NoTipskesehatanPage);
  }

  tipsDetail(item){
    this.navCtrl.push(NoTipskesehatanDetailPage, item);
  }

}
