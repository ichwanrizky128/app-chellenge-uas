import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-no-tipskesehatan-detail',
  templateUrl: 'no-tipskesehatan-detail.html',
})
export class NoTipskesehatanDetailPage {
  items: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items = this.navParams.data;
    console.log(this.items);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoTipskesehatanDetailPage');
  }

}
