import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoTipskesehatanDetailPage } from './no-tipskesehatan-detail';

@NgModule({
  declarations: [
    NoTipskesehatanDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(NoTipskesehatanDetailPage),
  ],
})
export class NoTipskesehatanDetailPageModule {}
