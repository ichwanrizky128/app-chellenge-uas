import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { listdokter } from '../../../model/m_tanyadokter';
import { PTanyadokterProvider } from '../../../providers/p-tanyadokter/p-tanyadokter';

@IonicPage()
@Component({
  selector: 'page-no-tanyadokter',
  templateUrl: 'no-tanyadokter.html',
})
export class NoTanyadokterPage {
  public list_dokter: listdokter[] = [];
  caridokter:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public tanyadokterProvider: PTanyadokterProvider) {
  }

  ngOnInit(){
    this.getListdokter();
  }

  getListdokter(){
    this.list_dokter = [];
    this.tanyadokterProvider.getListdokter().subscribe((result) => {
      console.log(result);
      this.list_dokter = result;
    });
    console.log(this.list_dokter);
  }


  toggleSection(i){
    this.list_dokter[i].open = !this.list_dokter[i].open;
  }

  getItems(ev){
    let val = ev.target.value;
    if (!val || !val.trim()){
      this.getListdokter();
    }
    else{
      this.list_dokter = [];
      this.tanyadokterProvider.cariDokter(val).subscribe((result) => {
        console.log(result);
        this.list_dokter = result;
      });
    }
  }

}
