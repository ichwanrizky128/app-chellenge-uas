import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoTanyadokterPage } from './no-tanyadokter';

@NgModule({
  declarations: [
    NoTanyadokterPage,
  ],
  imports: [
    IonicPageModule.forChild(NoTanyadokterPage),
  ],
})
export class NoTanyadokterPageModule {}
