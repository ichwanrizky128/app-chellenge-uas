import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoForumdiskusiPage } from './no-forumdiskusi';

@NgModule({
  declarations: [
    NoForumdiskusiPage,
  ],
  imports: [
    IonicPageModule.forChild(NoForumdiskusiPage),
  ],
})
export class NoForumdiskusiPageModule {}
