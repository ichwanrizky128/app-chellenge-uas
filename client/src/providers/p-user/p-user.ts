import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs';
import { User } from '../../model/m_user';


@Injectable()
export class PUserProvider {
  public user: User[] =[];

  constructor(public http: Http) {
    console.log('Hello PUserProvider Provider');
  }

  getUser(){
    return this.http.get("http://localhost:8081/api/user").map((response: Response) => {
      let data = response.json();
      this.user = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

  getUser2(user_email: String, user_password: String){
    return this.http.get("http://localhost:8081/api/login/"+user_email+"/"+user_password).map((response: Response) => {
      let data = response.json();
      this.user = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

  getUser3(user_id: String){
    return this.http.get("http://localhost:8081/api/user/"+user_id).map((response: Response) => {
      let data = response.json();
      this.user = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

  addUser(data){
    var url ="http://localhost:8081/api/user";
    return this.http.post(url, data)
  }

  updateProfile(user_id, data){
    var url ="http://localhost:8081/api/user/"+user_id;
    return this.http.post(url, data)
  }

  updatePassword(user_id, data){
    var url ="http://localhost:8081/api/changepassword/"+user_id;
    return this.http.post(url, data)
  }

  updateImg(user_id, data){
    var url ="http://localhost:8081/api/changeimg/"+user_id;
    return this.http.post(url, data)
  }

}
