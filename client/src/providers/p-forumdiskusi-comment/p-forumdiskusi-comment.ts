import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs';
import { forumdiskusi_comment } from '../../model/m_forumdiskusi_comment';

@Injectable()
export class PForumdiskusiCommentProvider {
  public forumdiskusi_comment: forumdiskusi_comment[] = [];
  constructor(public http: Http) {
    console.log('Hello PForumdiskusiCommentProvider Provider');
  }

  getComment(id_diskusi: string){
    return this.http.get("http://localhost:8081/api/comment/"+id_diskusi).map((response: Response) => {
      let data = response.json();
      this.forumdiskusi_comment = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

  addComment(data){
    var url ="http://localhost:8081/api/comment";
    return this.http.post(url, data)
  }
}
