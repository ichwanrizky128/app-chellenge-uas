var pool = require('./databaseConfig.js');
var aplikasiSehatDB = {

    getDokter: function (callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_listdokter ORDER BY (jenis_dokter) ASC';
                conn.query(sql, function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
    ,
    cariDokter: function (nama_dokter,jenis_dokter, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_listdokter where nama_dokter LIKE ? OR jenis_dokter like ?';
                conn.query(sql,[nama_dokter,jenis_dokter], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
    ,
    addTanyaDokter: function (tanya_id, namadokter, namapenanya, isipertanyaan, jawabanpertanyaan, fotodokter, fotopenanya, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'INSERT INTO tb_tanya_dokter (tanya_id, namadokter, namapenanya, isipertanyaan, jawabanpertanyaan, fotodokter, fotopenanya) VALUES (?,?,?,?,?,?,?)';
                conn.query(sql,[tanya_id, namadokter, namapenanya, isipertanyaan, jawabanpertanyaan, fotodokter, fotopenanya], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
    ,
    getTanyaDokter1: function (namapenanya, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_tanya_dokter where namapenanya=? ORDER BY (tanya_id) DESC';
                conn.query(sql,[namapenanya], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
};
module.exports = aplikasiSehatDB