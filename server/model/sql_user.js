var pool = require('./databaseConfig.js');
var aplikasiSehatDB = {

    getUser: function (callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_user';
                conn.query(sql,function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
,
    getUser2: function (user_email, user_password, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_user where user_email = ? AND user_password = ?';
                conn.query(sql, [user_email, user_password], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
,
    getUser3: function (user_id, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_user where user_id = ?';
                conn.query(sql,[user_id],function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
,
    addUser: function (user_id, user_nama, user_email, user_password, user_img, user_hakakses, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'INSERT INTO tb_user (user_id, user_nama, user_email, user_password, user_img, user_hakakses) VALUES (?,?,?,?,?,?)';
                conn.query(sql,[user_id, user_nama, user_email, user_password, user_img, user_hakakses],function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
,

    UpdateUser: function (user_nama, user_email, user_id, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'UPDATE tb_user SET user_nama=?, user_email=? WHERE user_id=? ';
                conn.query(sql,[user_nama, user_email, user_id],function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
,
    UpdatePassword: function (user_password, user_id, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'UPDATE tb_user SET user_password=? WHERE user_id=? ';
                conn.query(sql,[user_password, user_id],function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
,
    updateImg: function (user_img, user_id, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'UPDATE tb_user SET user_img=? WHERE user_id=? ';
                conn.query(sql,[user_img, user_id],function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }

};
module.exports = aplikasiSehatDB