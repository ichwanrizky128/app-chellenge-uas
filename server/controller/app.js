var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();

var path = require('path');
var cors = require("cors")
var cor = cors();
app.use(cor);   
app.use(express.static(path.join(__dirname, "../public")));

/** Forum Diskusi*/
var server_diskusi = require('../model/sql_forumdiskusi.js');
app.get('/api/diskusi', function (req, res){
    server_diskusi.getDiskusi(function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.get('/api/diskusi/:email', function (req, res){
    var email = req.params.email;
    server_diskusi.getDiskusiByEmail(email, function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.post('/api/diskusi', urlencodedParser, jsonParser, function (req, res) {
    var id_diskusi = req.body.id_diskusi;
    var nama = req.body.nama;
    var email = req.body.email;
    var tanggal = req.body.tanggal;
    var diskusi = req.body.diskusi;
    var foto = req.body.foto;

    server_diskusi.addDiskusi(id_diskusi, nama, email, tanggal, diskusi, foto, function(err, result) {
        if (!err){
            console.log(result);
            res.send(result.affectedRows + 'data berhasil ');
        }
        else {
            console.log(err);
            res.status(500).send(err.code);
        }
    });
});
app.get('/api/comment/:id_diskusi', function (req, res){
    var id_diskusi = req.params.id_diskusi;
    server_diskusi.getComment(id_diskusi,function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.post('/api/comment', urlencodedParser, jsonParser, function (req, res) {
    var diskusi_reply_id = req.body.diskusi_reply_id;
    var diskusi_reply_nama = req.body.diskusi_reply_nama;
    var diskusi_reply_isi = req.body.diskusi_reply_isi;
    var diskusi_reply_foto = req.body.diskusi_reply_foto;
    var id_diskusi = req.body.id_diskusi;

    server_diskusi.addComment(diskusi_reply_id, diskusi_reply_nama, diskusi_reply_isi, diskusi_reply_foto, id_diskusi, function(err, result) {
        if (!err){
            console.log(result);
            res.send(result.affectedRows + 'data berhasil ');
        }
        else {
            console.log(err);
            res.status(500).send(err.code);
        }
    });
});
app.delete('/api/deletepost/:id_diskusi',urlencodedParser, jsonParser, function (req, res){
    var id_diskusi = req.params.id_diskusi;
    server_diskusi.deleteDiskusi(id_diskusi, function(err, result){
        if(!err){
            res.send(result.affectedRows + 'post Dihapus ');
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});

/** Tanya Dokter*/
var server_dokter = require('../model/sql_tanyadokter.js');
app.get('/api/listdokter', function (req, res){
    server_dokter.getDokter(function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.get('/api/listdokter/:cari', function (req, res){
    var nama_dokter = req.params.cari
    var jenis_dokter = req.params.cari
    server_dokter.cariDokter(nama_dokter,jenis_dokter, function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.get('/api/tanyadokter/:nama', function (req, res){
    var namapenanya =  req.params.nama
    server_dokter.getTanyaDokter1(namapenanya, function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.post('/api/tanyadokter', urlencodedParser, jsonParser, function (req, res){
    var tanya_id = req.body.tanya_id;
    var namadokter = req.body.namadokter;
    var namapenanya = req.body.namapenanya;
    var isipertanyaan = req.body.isipertanyaan;
    var jawabanpertanyaan = req.body.jawabanpertanyaan;
    var fotodokter = req.body.fotodokter;
    var fotopenanya = req.body.fotopenanya;

    server_dokter.addTanyaDokter(tanya_id, namadokter, namapenanya, isipertanyaan, jawabanpertanyaan, fotodokter, fotopenanya, function(err, result){
        if(!err){
            console.log(result);
            res.send(result.affectedRows + 'data berhasil ');
            
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});

/** Tips Kesehatan*/
var server_tipskesehatan = require('../model/sql_tipskesehatan.js');
app.get('/api/tips/:tips_category', function (req, res){
    var tips_category = req.params.tips_category;
    server_tipskesehatan.getTipskesehatan(tips_category, function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});

/** User*/
var server_user = require('../model/sql_user.js');
app.get('/api/user/', function (req, res){
    server_user.getUser(function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.get('/api/login/:user_email/:user_password',urlencodedParser, jsonParser, function (req, res){
    var user_email = req.params.user_email;
    var user_password = req.params.user_password;
    server_user.getUser2(user_email, user_password, function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
        }
    });
});
app.get('/api/user/:user_id',urlencodedParser, jsonParser, function (req, res){
    var user_id = req.params.user_id;
    server_user.getUser3(user_id, function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
        }
    });
});
app.post('/api/user/',urlencodedParser, jsonParser, function (req, res){
    var user_id = req.body.user_id;
    var user_nama = req.body.user_nama;
    var user_email = req.body.user_email;
    var user_password = req.body.user_password;
    var user_img = req.body.user_img;
    var user_hakakses = req.body.user_hakakses;
    server_user.addUser(user_id, user_nama, user_email, user_password, user_img, user_hakakses, function(err, result){
        if(!err){
            res.send(result.affectedRows + 'data berhasil ');
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.post('/api/user/:user_id',urlencodedParser, jsonParser, function (req, res){
    var user_id = req.params.user_id;
    var user_nama = req.body.user_nama;
    var user_email = req.body.user_email;
    server_user.UpdateUser(user_nama, user_email, user_id, function(err, result){
        if(!err){
            res.send(result.affectedRows + 'data Diubah ');
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.post('/api/changepassword/:user_id',urlencodedParser, jsonParser, function (req, res){
    var user_id = req.params.user_id;
    var user_password = req.body.user_password;
    server_user.UpdatePassword(user_password, user_id, function(err, result){
        if(!err){
            res.send(result.affectedRows + 'data Diubah ');
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});
app.post('/api/changeimg/:user_id',urlencodedParser, jsonParser, function (req, res){
    var user_id = req.params.user_id;
    var user_img = req.body.user_img;
    server_user.updateImg(user_img, user_id, function(err, result){
        if(!err){
            res.send(result.affectedRows + 'data Diubah ');
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});

/** Artikel*/
var server_artikel = require('../model/sql_artikel.js');
app.get('/api/artikel/', function (req, res){
    server_artikel.getArtikel(function(err, result){
        if(!err){
            res.send(result);
        }
        else{
            console.log(err);
            res.status(500).send(err);
        }
    });
});

module.exports = app